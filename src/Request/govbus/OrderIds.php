<?php
/**
 * User:  MaxZhang
 * Email: q37388438@gmail.com
 * Date: 2019/06/10
 * Time: 19:38
 */

namespace MaxZhang\SuningSdk\Request\Govbus;

class OrderIds {

    private $apiParams = array();

    private $orderId;

    public function getOrderId() {
        return $this->orderId;
    }

    public function setOrderId($orderId) {
        $this->orderId = $orderId;
        $this->apiParams["orderId"] = $orderId;
    }

    public function getApiParams(){
        return $this->apiParams;
    }

}
