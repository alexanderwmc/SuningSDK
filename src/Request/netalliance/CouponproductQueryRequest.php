<?php
/**
 * User: MaxZhang
 * Email:q373884384@gmail.com
 * Date: 2019/6/10 0010
 * Time: 23:37
 */

namespace MaxZhang\SuningSdk\Request\netalliance;


use MaxZhang\SuningSdk\SuningRequest;

class CouponproductQueryRequest extends SuningRequest
{


    /**
     *推广位
     */
    private $positionId;



    public function getPositionId() {
        return $this->positionId;
    }

    /**
     * @param $positionId 推广位ID
     */
    public function setPositionId($positionId) {
        $this->positionId = $positionId;
        $this->apiParams["positionId"] = $positionId;
    }

    public function getApiMethodName(){
        return 'suning.netalliance.couponproduct.query';
    }

    public function getApiParams(){
        return $this->apiParams;
    }

    public function check(){
        //非空校验
        RequestCheckUtil::checkNotNull($this->pageNo, 'pageNo');
        RequestCheckUtil::checkNotNull($this->positionId, 'positionId');
    }

    public function getBizName(){
        return "queryCouponproduct";
    }
}