<?php
/**
 * User: MaxZhang
 * Email:q373884384@gmail.com
 * Date: 2019/6/10 0010
 * Time: 23:16
 */

namespace MaxZhang\SuningSdk\Request\custom;


use MaxZhang\SuningSdk\SuningRequest;

/**
 * 团长招商批量查询联盟商品信息
 * Class CustnumandcommdityQueryRequest
 * @package MaxZhang\SuningSdk\Request\custom
 */
class CustnumandcommdityQueryRequest extends SuningRequest
{

    /**
     * 根据请求方式，生成相应请求报文
     *
     * @param
     *            type 请求方式(json或xml)
     */
    function getApiParams()
    {
        return $this->apiParams;
    }

    /**
     * 获取接口方法名称
     */
    function getApiMethodName()
    {
        return "suning.netalliance.custnumandcommdity.query";
    }

    /**
     * 数据校验
     */
    function check()
    {
        // TODO: Implement check() method.
    }

    function getBizName()
    {
        return "queryCustnumandcommdity";
    }
}