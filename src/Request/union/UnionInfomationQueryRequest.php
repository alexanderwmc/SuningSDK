<?php
/**
 * User: MaxZhang
 * Email:q373884384@gmail.com
 * Date: 2019/6/10 0010
 * Time: 20:44
 */

namespace MaxZhang\SuningSdk\Request\union;

use MaxZhang\SuningSdk\SelectSuningRequest;

/**
 * 苏宁开放平台接口 - 批量查询联盟商品信息
 * Class UnionInfomationQueryRequest
 * @package MaxZhang\SuningSdk\Request\union
 */
class UnionInfomationQueryRequest extends SelectSuningRequest
{
    public function getApiMethodName()
    {
        return 'suning.netalliance.unioninfomation.query';
    }

    public function getApiParams()
    {
        return $this->apiParams;
    }

    public function check()
    {
        //非空校验
    }

    public function getBizName()
    {
        return "queryUnionInfomation";
    }
}