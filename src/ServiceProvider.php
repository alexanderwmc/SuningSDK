<?php
/**
 * User:  MaxZhang
 * Email: q37388438@gmail.com
 * Date: 2019/06/10
 * Time: 18:35
 */

namespace MaxZhang\SuningSdk;


class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected $defer = true;

    public function register()
    {
        $this->app->singleton(DefaultSuningClient::class, function () {
            return new DefaultSuningClient(
                config('services.suningSdk.serverUrl'),
                config('services.suningSdk.appKey'),
                config('services.suningSdk.appSecret'),
                config('services.suningSdk.format')
            );
        });

        $this->app->alias(DefaultSuningClient::class, 'suningSdk');
    }

    public function provides()
    {
        return [DefaultSuningClient::class, 'suningSdk'];
    }
}