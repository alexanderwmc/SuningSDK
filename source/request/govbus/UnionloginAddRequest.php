<?php
/**
 * 苏宁开放平台接口 - 
 *
 * @author suning
 * @date   2019-3-5
 */
class UnionloginAddRequest  extends SuningRequest{
	
	/**
	 * 
	 */
	private $comabb;
	
	/**
	 * 
	 */
	private $comName;
	
	/**
	 * 
	 */
	private $extra;
	
	/**
	 * 
	 */
	private $sysCode;
	
	/**
	 * 
	 */
	private $targetUrl;
	
	/**
	 * 
	 */
	private $uid;
	
	public function getComabb() {
		return $this->comabb;
	}
	
	public function setComabb($comabb) {
		$this->comabb = $comabb;
		$this->apiParams["comabb"] = $comabb;
	}
	
	public function getComName() {
		return $this->comName;
	}
	
	public function setComName($comName) {
		$this->comName = $comName;
		$this->apiParams["comName"] = $comName;
	}
	
	public function getExtra() {
		return $this->extra;
	}
	
	public function setExtra($extra) {
		$this->extra = $extra;
		$this->apiParams["extra"] = $extra;
	}
	
	public function getSysCode() {
		return $this->sysCode;
	}
	
	public function setSysCode($sysCode) {
		$this->sysCode = $sysCode;
		$this->apiParams["sysCode"] = $sysCode;
	}
	
	public function getTargetUrl() {
		return $this->targetUrl;
	}
	
	public function setTargetUrl($targetUrl) {
		$this->targetUrl = $targetUrl;
		$this->apiParams["targetUrl"] = $targetUrl;
	}
	
	public function getUid() {
		return $this->uid;
	}
	
	public function setUid($uid) {
		$this->uid = $uid;
		$this->apiParams["uid"] = $uid;
	}
	
	public function getApiMethodName(){
		return 'suning.govbus.unionlogin.add';
	}
	
	public function getApiParams(){
		return $this->apiParams;
	}
	
	public function check(){
		//非空校验
		RequestCheckUtil::checkNotNull($this->sysCode, 'sysCode');
		RequestCheckUtil::checkNotNull($this->uid, 'uid');
	}
	
	public function getBizName(){
		return "addUnionlogin";
	}
	
}

?>